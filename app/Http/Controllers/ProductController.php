<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\Product;
use App\Models\User;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return Response()->json(['products'=>Product::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $data = Validator::make(
            $request->all(),
            [
                'name' => 'required|string|max:255',
                'price' => 'required|integer|min:1',
                'image'=> 'required|image',
                'type' => 'required|in:laptop,phone'
            ]
        );

        if($data->fails())
            return Response()->json(['errors'=>$data->errors()]);



        if ($request->hasFile('image')) {
            // Get filename with extension
            $filenameWithExt = $request->file('image')->getClientOriginalName();

            // Get just the filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);

            // Get extension
            $extension = $request->file('image')->getClientOriginalExtension();

            // Create new filename
            $filenameToStore = $filename.'_'.time().'.'.$extension;

            // Uplaod image
            $path = $request->file('image')->storeAs('images', $filenameToStore);


            $request['user_id'] = Auth::user()->id;

            $product = Product::create(
                [
                    'name' => $request['name'],
                    'image' => URL::asset('storage/'.$path),
                    'price' => (int)$request['price'],
                    'user_id' => Auth::user()->id,
                    'type' => $request['type']
                ]
            );
            $product->save();
            $users = User::all();
            foreach ($users as $user){
                Notification::create(
                    [
                        'user_id' => $user->id,
                        'content' => Auth::user()->name.' added a new product ('.$request['type'].').'
                    ]
                );
            }
            return Response()->json(['product'=>$product]);
        }



        return Response()->json(['error'=>'Image Error']);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $product = Product::find($id);
        if($product == null)
            return Response()->json(['error'=>'product not found']);
        return Response()->json(['product'=>$product]);
    }

    public function search($name){
        $product = DB::table('products')
            ->where('name', 'like', '%'.$name.'%')
            ->get();
        return Response()->json(['products'=>$product]);
    }

    public function type($type){

        $product = DB::table('products')
            ->where('type', '=', $type)
            ->get();
        return Response()->json(['product'=>$product]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if($product->user_id != Auth::user()->id)
            return Response()->json(['error'=>'this product is not belong to you.']);
        $product->delete();
        return Response()->json(['message'=>'product deleted successfully.']);
    }
}
