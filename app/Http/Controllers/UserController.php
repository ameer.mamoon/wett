<?php

namespace App\Http\Controllers;

use App\Models\User;
use Exception;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;




class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return Response()->json(['users'=>User::all()]);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $data = Validator::make(
            $request->all(),
            [
                'name' => 'required|string|max:255',
                'email'=> 'required|email|max:255',
                'password'=> 'required|string|max:50',
                'phone' => 'required|string|max:20'
            ]
        );

        if($data->fails())
            return Response()->json(['errors'=>$data->errors()]);

        try{
            $user = User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'password' => Hash::make($request['password']),
                'phone' => $request['phone']
            ]);
        }
        catch (Exception $exception){
            return Response()->json(['error'=>'email is invalid.']);
        }


        $token = $user->createToken('authToken')->plainTextToken;
        $user->save();
        return Response()->json([
            'user'=>$user,
            'token' => $token,
            ]);
    }

    public function login(Request $request){
        if (!Auth::attempt($request->only('email', 'password'))) {
            return response()->json([
                'error' => 'Invalid login details'
            ], 401);
        }

        $user = User::where('email', $request['email'])->firstOrFail();

        $token = $user->createToken('authToken')->plainTextToken;

        return response()->json([
            'token' => $token,
            'user' => $user
        ]);


    }

    public function logout(){
        try{
            Auth::user()->tokens->each(function ($token, $key) {
                $token->delete();
            });
            return Response()->json(['message'=>'log out successfully.']);
        }catch (Exception $exception){
            return Response()->json(['error'=>'log out failed.']);
        }
    }

    public function update(Request $request)
    {

        $data = Validator::make(
            $request->all(),
            [
                'name' => 'string|max:255',
                'email'=> 'email|max:255',
                'password'=> 'string|max:50',
                'phone' => 'string|min:10|max:20'
            ]
        );

        if($data->fails())
            return Response()->json(['error'=>$data->errors()]);

        if($request['password'] != null)
            $request['password'] = Hash::make($request['password']);

        $user = User::find(Auth::user()->id);
        $user->update($request->all());
        $user->save();
        return Response()->json([
            'user'=>$user,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
