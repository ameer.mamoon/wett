<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WishListController;
use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware'=>['auth:sanctum']],
    function (){

        Route::put('/profile',[UserController::class,'update']);
        Route::get('/logout',[UserController::class,'logout']);

        Route::prefix('/products')->group(function (){
            Route::get('/',[ProductController::class,'index']);
            Route::post('/add',[ProductController::class,'store']);
            Route::get('/{id}',[ProductController::class,'show']);
            Route::get('/search/{name}',[ProductController::class,'search']);
            Route::get('/type/{type}',[ProductController::class,'type']);
            Route::delete('/delete/{id}',[ProductController::class,'destroy']);
        });

        Route::prefix('/cart')->group(function (){
            Route::post('/add/{id}',[CartController::class,'store']);
            Route::get('/all',[CartController::class,'index']);
            Route::delete('/delete',[CartController::class,'destroy']);
        });

        Route::prefix('/wishlist')->group(function (){
            Route::post('/add/{id}',[WishListController::class,'store']);
            Route::get('/all',[WishListController::class,'index']);
            Route::delete('/delete',[WishListController::class,'destroy']);

        });

        Route::get('/notifications',[NotificationController::class,'index']);
    }
);

Route::prefix('auth')->group(function (){
    Route::post('/register',[UserController::class,'store']);
    Route::post('/login',[UserController::class,'login']);
});
